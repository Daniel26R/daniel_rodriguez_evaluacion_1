const form = document.getElementById("formulario");
const correo = document.getElementById("email");
const nombre = document.getElementById("nombre");
const rut = document.getElementById("validRut");
const fecha = document.getElementById("fecha");
const numero = document.getElementById("numero");

form.addEventListener('submit', e =>{
    e.preventDefault();
    checkInpust();
});

function checkInpust(){
    const correoValue = email.value.trim();
    const nombreValue = nombre.value.trim();
    const rutValue = validRut.value.trim();
    const fechaValue = fecha.value.trim();
    const numeroValue = numero.value.trim();

    if (nombreValue === ''){
        setErrorFor(nombre,"No puede quedar en blanco");
    }else{
        setSuccesFor(nombre);
    }
        
    
}

function setErrorFor(Input, message){
    const formControl = input.parentElement;
    const small = formControl.querySelector('small');
    formControl.className = 'form-control error';
    small.btn-primary = message;
}

function setSuccessFor(input) {
	const formControl = input.parentElement;
	formControl.className = 'form-control success';
}

function isEmail(email) {
	return /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(email);
}
